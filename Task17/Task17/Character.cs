﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    class Character
    {
        //Setting and getting all the values
        private string name;
        private string class1;
        private string subclass;
        private int hP;
        private int mana;
        private int armor;

        public string Name { get => name; set => name = value; }
        public string Class1 { get => class1; set => class1 = value; }
        public string Subclass { get => subclass; set => subclass = value; }
        public int HP { get => hP; set => hP = value; }
        public int Mana { get => mana; set => mana = value; }
        public int Armor { get => armor; set => armor = value; }

        public Character(string name, string class1, string subclass, int hP, int mana, int armor)
        {
            Name = name;
            Class1 = class1;
            Subclass = subclass;
            HP = hP;
            Mana = mana;
            Armor = armor;
        }
        public Character(string name, string class1, int hP, int mana, int armor)
        {
            Name = name;
            Class1 = class1;
            HP = hP;
            Mana = mana;
            Armor = armor;
        }
        public string GetInfo(string name)
        {
            string info = "no info";
            return info;
        }
        
        public void Attack()
        {
            
        }
        public void Move()
        {

        }
    }
}
